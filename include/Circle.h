#ifndef CIRCLE_H
#define CIRCLE_H
// Still the most portable way to define pi.
#define PI 3.14159265359

#include "Shape.h"
#include "Movable.h"

class Circle : public Shape, public Movable
{
protected:
    unsigned int radius;

    virtual std::string typeString() const;
public:
    Circle(unsigned int leftTopX, unsigned int leftTopY, unsigned int radius);
    virtual void calculatePoints();
    virtual void calculateArea();
    virtual void calculatePerimeter();

    virtual void scale(double scaleX, double scaleY);
    virtual void move(unsigned int newX, unsigned int newY);
};
#endif
