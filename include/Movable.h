#ifndef MOVABLE_H
#define MOVABLE_H
class Movable
{
public:
    // IMO, move should be implemented in Movable because all subclasses will implement it the same way, but I was advised to keep it as an interface.
    virtual void move(unsigned int newX, unsigned int newY) = 0;
    virtual void scale(double scaleX, double scaleY) = 0;
};
#endif
