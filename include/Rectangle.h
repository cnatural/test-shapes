#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Shape.h"
#include "Movable.h"

class Rectangle : public Shape, public Movable
{
protected:
    unsigned int height, width;

    virtual std::string typeString() const;
public:
    Rectangle(unsigned int leftTopX, unsigned int leftTopY, unsigned int height, unsigned int width);
    virtual void calculatePoints();
    virtual void calculateArea();
    virtual void calculatePerimeter();

    virtual void scale(double scaleX, double scaleY);
    virtual void move(unsigned int newX, unsigned int newY);
};
#endif
