#ifndef POINT_H
#define POINT_H
class Point
{
private:
    unsigned int x, y;
public:
    Point() {} // Code will not compile without this declaration. VS doesn't even say this is the problem.
    Point(unsigned int x, unsigned int y);
    unsigned int getX() const;
    unsigned int getY() const;
    void move(unsigned int x, unsigned int y);
};
#endif
