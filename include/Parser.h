#ifndef PARSER_H
#define PARSER_H
#include <string>
#include <cstring>
#include <vector>
#include <limits>
#include <sstream>
#include <exception>

void parseCommand(
        char const *cstr,
        std::vector<std::string> &parameters);

void parseCommand(
        std::string const &userCommand,
        std::vector<std::string> &parameters);

unsigned int stou(std::string const &str, size_t *pos = nullptr, int base = 10);

void assignParameters(
        std::vector<std::string>::iterator itParam,
        std::vector<std::string>::iterator end,
        unsigned int &param);

void assignParameters(
        std::vector<std::string>::iterator itParam,
        std::vector<std::string>::iterator end,
        double &param);

template <typename T, typename... Args>
void assignParameters(
        std::vector<std::string>::iterator itParam,
        std::vector<std::string>::iterator end,
        T &firstParam,
        Args &... params)
{
    // This implementation must be put in the header file, as templates are fickle.
    // This is how the STL does it, so it should be fine.

    // Note: While it seems like assignParameters for double and uint have a
    // lot of overlapping exception-handling code, putting it in here will
    // break the program at edge cases.

    assignParameters(itParam, end, firstParam);
    assignParameters(++itParam, end, params...);
}
#endif
