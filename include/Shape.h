#ifndef SHAPE_H
#define SHAPE_H
#include <string>
#include <list>
#include <iostream>

#include "Point.h"

class Shape
{
protected:
    Point leftTop, rightTop, rightBottom, leftBottom;
    std::list<Point*> points;

    bool isCircular;

    double area;
    double perimeter;

    virtual std::string typeString() const = 0;
public:
    Shape(unsigned int leftTopX, unsigned int leftTopY, bool const isCircular = false);
    virtual ~Shape() { }
    virtual void calculatePoints() = 0;
    virtual void calculateArea() = 0;
    virtual void calculatePerimeter() = 0;
    virtual std::string toString() const;
};

std::ostream &operator<<(std::ostream &os, Shape const *shape);
#endif
