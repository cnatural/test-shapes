#ifndef HELPERS_H
#define HELPERS_H
unsigned int addSafe(unsigned int lhs, unsigned int rhs);
unsigned int dtouSafe(double d);
#endif
