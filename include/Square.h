#ifndef SQUARE_H
#define SQUARE_H
#include "Shape.h"
#include "Movable.h"

class Square : public Shape, public Movable
{
protected:
    unsigned int edge;

    virtual std::string typeString() const;
public:
    Square(unsigned int leftTopX, unsigned int leftTopY, unsigned int edge);
    virtual void calculatePoints();
    virtual void calculateArea();
    virtual void calculatePerimeter();

    virtual void scale(double scaleX, double scaleY);
    virtual void move(unsigned int newX, unsigned int newY);
};
#endif
