#include "Square.h"
#include "Helpers.h"

#include <exception>
#include <cmath>
#include <sstream>

Square::Square(unsigned int leftTopX, unsigned int leftTopY, unsigned int edge) : Shape(leftTopX, leftTopY)
{
    this->edge = edge;

    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        throw std::out_of_range("Cannot create a shape with these attributes, as it will hit the max boundary.");
    }

    this->calculateArea();
    this->calculatePerimeter();
}

void Square::calculateArea()
{
    this->area = this->edge * this->edge;
}

void Square::calculatePerimeter()
{
    this->perimeter = this->edge * 4;
}

void Square::calculatePoints()
{
    unsigned int x = this->leftTop.getX();
    unsigned int y = this->leftTop.getY();

    this->rightTop.move(addSafe(x, this->edge), y);
    this->leftBottom.move(x, addSafe(y, this->edge));
    this->rightBottom.move(addSafe(x, this->edge), addSafe(y, this->edge));
}

std::string Square::typeString() const
{
    std::stringstream out;
    out << "Square[e=" << this->edge << "]" << std::endl;
    return out.str();
}

void Square::scale(double scaleX, double scaleY)
{
    if (scaleX != scaleY)
    {
        throw std::domain_error("Scaling a square must be isotropic.");
    }

    unsigned int tempEdge = this->edge;
    try
    {
        this->edge = dtouSafe(std::round(scaleX * (double)this->edge));
    }
    catch (std::out_of_range &e)
    {
        throw std::out_of_range("Cannot scale by this amount as result of multiplying scaling factor by length will exceed range of unsigned int.");
    }

    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        this->edge = tempEdge; // Reset edge if change is invalid.
        this->calculatePoints(); // Revert any changed points.
        throw std::out_of_range("Cannot scale by this amount, or shape will hit max boundary.");
    }

    this->calculatePerimeter();
    this->calculateArea();
}

void Square::move(unsigned int newX, unsigned int newY)
{
    // Note: Duplicate of other move implementations. Read comment in Movable header.
    // Create temporary copy of leftTop in case move is invalid.
    Point temp = this->leftTop;
    this->leftTop.move(newX, newY);
    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        this->leftTop = temp;
        this->calculatePoints();
        throw std::out_of_range("Cannot move leftTop to these coordinates, or shape will hit max boundary.");
    }
}
