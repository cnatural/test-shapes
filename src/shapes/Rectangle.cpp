#include "Rectangle.h"
#include "Helpers.h"

#include <sstream>
#include <cmath>

Rectangle::Rectangle(unsigned int leftTopX, unsigned int leftTopY, unsigned int height, unsigned int width) : Shape(leftTopX, leftTopY)
{
    this->height = height;
    this->width = width;

    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        throw std::out_of_range("Cannot create a shape with these attributes, as it will hit the max boundary.");
    }

    this->calculateArea();
    this->calculatePerimeter();
}

void Rectangle::calculateArea()
{
    this->area = this->height * this->width;
}

void Rectangle::calculatePerimeter()
{
    this->perimeter = (this->height + this->width) * 2;
}

void Rectangle::calculatePoints()
{
    unsigned int x = this->leftTop.getX();
    unsigned int y = this->leftTop.getY();

    this->rightTop.move(addSafe(x, this->width), y);
    this->leftBottom.move(x, addSafe(y, this->height));
    this->rightBottom.move(addSafe(x, this->width), addSafe(y, this->height));
}

std::string Rectangle::typeString() const
{
    std::stringstream out;
    out << "Rectangle[h=" << this->height << ",w=" << this->width << "]" << std::endl;
    return out.str();
}

void Rectangle::scale(double scaleX, double scaleY)
{
    // Create temporary copies of lengths in case scaling is invalid.
    unsigned int tempWidth = this->width;
    unsigned int tempHeight = this->height;
    // Explicit cast here just for clarification, this is valid implicitly too.
    try
    {
        this->width = dtouSafe(std::round(scaleX * (double)this->width));
        this->height = dtouSafe(std::round(scaleY * (double)this->height));
    }
    catch (std::out_of_range &e)
    {
        throw std::out_of_range("Cannot scale by this amount as result of multiplying scaling factor by lengths will exceed range of unsigned int.");
    }

    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        // Reset width and height if change is invalid.
        this->width = tempWidth;
        this->height = tempHeight;
        this->calculatePoints(); // Revert any changed points.
        throw std::out_of_range("Cannot scale by this amount, or shape will hit max boundary.");
    }

    this->calculateArea();
    this->calculatePerimeter();
}

void Rectangle::move(unsigned int newX, unsigned int newY)
{
    // Note: Duplicate of other move implementations. Read comment in Movable header.
    // Create temporary copy of leftTop in case move is invalid.
    Point temp = this->leftTop;
    this->leftTop.move(newX, newY);
    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        this->leftTop = temp;
        this->calculatePoints();
        throw std::out_of_range("Cannot move leftTop to these coordinates, or shape will hit max boundary.");
    }
}
