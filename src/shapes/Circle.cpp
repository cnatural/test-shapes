#include "Circle.h"
#include "Helpers.h"

#include <exception>
#include <cmath>
#include <sstream>

Circle::Circle(unsigned int leftTopX, unsigned int leftTopY, unsigned int radius) : Shape(leftTopX, leftTopY, true)
{
    this->radius = radius;

    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        throw std::out_of_range("Cannot create a shape with these attributes, as it will hit the max boundary.");
    }

    this->calculateArea();
    this->calculatePerimeter();
}

void Circle::calculateArea()
{
    this->area = PI * this->radius * this->radius;
}

void Circle::calculatePerimeter()
{
    this->perimeter = 2 * PI * this->radius;
}

void Circle::calculatePoints()
{
    unsigned int x = this->leftTop.getX();
    unsigned int y = this->leftTop.getY();

    this->rightBottom.move(addSafe(x, (2 * this->radius)), addSafe(y, (2 * this->radius)));
}

std::string Circle::typeString() const
{
    std::stringstream out;
    out << "Circle[r=" << this->radius << "]" << std::endl;
    return out.str();
}

void Circle::scale(double scaleX, double scaleY)
{
    if (scaleX != scaleY)
    {
        throw std::domain_error("Scaling a circle must be isotropic.");
    }

    unsigned int tempRadius = this->radius;
    try
    {
        this->radius = dtouSafe(std::round(scaleX * (double)this->radius));
    }
    catch (std::out_of_range &e)
    {
        throw std::out_of_range("Cannot scale by this amount as result of multiplying scaling factor by radius will exceed range of unsigned int.");
    }

    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        this->radius = tempRadius; // Reset radius if change is invalid.
        this->calculatePoints(); // Revert changed point.
        throw std::out_of_range("Cannot scale by this amount, or shape will hit max boundary.");
    }

    this->calculatePerimeter();
    this->calculateArea();
}

void Circle::move(unsigned int newX, unsigned int newY)
{
    // Note: Duplicate of other move implementations. Read comment in Movable header.
    // Create temporary copy of leftTop in case move is invalid.
    Point temp = this->leftTop;
    this->leftTop.move(newX, newY);
    try
    {
        this->calculatePoints();
    }
    catch (std::out_of_range &e)
    {
        this->leftTop = temp;
        this->calculatePoints();
        throw std::out_of_range("Cannot move leftTop to these coordinates, or shape will hit max boundary.");
    }
}
