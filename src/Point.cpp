#include "Point.h"

Point::Point(unsigned int x, unsigned int y)
{
    this->move(x, y);
}

unsigned int Point::getX() const
{
    return x;
}

unsigned int Point::getY() const
{
    return y;
}

void Point::move(unsigned int x, unsigned int y)
{
    this->x = x;
    this->y = y;
}
