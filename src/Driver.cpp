/* ------------------------------------------------------
CMP2801M: Advanced Programming
Driver program for assignment
Fall 2019

Written by Ayse Kucukyilmaz

This file is a representative test file.
During marking, we will use the exact same notation
as provided in the brief, so make sure
you follow that guideline. Also make sure that you don't
change the main body provided to you here.
Otherwise, your code may not pass the test cases...

GOOD LUCK!

------------------------------------------------------ */

#define __STDC_WANT_LIB_EXT1__ 1

#include "Shape.h"
#include "Movable.h"
#include "Rectangle.h"
#include "Square.h"
#include "Circle.h"
#include "Parser.h"

#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <exception>

using namespace std;

Shape *findShape(vector<Shape*> shapes, unsigned int shapeNo)
{
    try
    {
        return shapes.at(shapeNo - 1);
    }
    catch (out_of_range &e)
    {
        stringstream what;
        what << "No shape at index " << shapeNo;
        throw out_of_range(what.str());
    }
}

int main()
{
    string userCommand;
    vector <Shape*> shapes;     // this one will hold your shapes
    vector <string> parameters; // this one will hold parameters for the commands

    while (userCommand.compare("exit") != 0)
    {
        cout << "Enter the command: ";

        getline(cin, userCommand);

        // Problem: gcc and clang don't define strcpy_s and strtok_s, and I can't use VC++ at home cos Linux.
        // Solution: rather than create my own implementation/use unsafe functions/add a library, implement a string-based alternative.
        // If compiled with support for _s functions, prefer the cstring implementation, to show that we can use cstrings.
#if defined(_MSC_VER) || defined(__STDC_LIB_EXT1__)
        // If strcpy_s and friends are defined, use the recommended strtok_s implementation.
        char *cstr = new char[userCommand.length() + 1];
        if (strcpy_s(cstr, userCommand.length()+1, userCommand.c_str()))
        {
            cerr << "Could not copy command " << userCommand;
            delete cstr;
            continue;
        }
        parseCommand(cstr, parameters);
        delete cstr;

#else
        // If strcpy_s isn't defined, rather than use unsafe functions like strcpy, we use C++ strings to provide safety for us.
        parseCommand(userCommand, parameters);
#endif

        // We use `at` for bounds checking, to prevent undefined behaviour.
        string command;
        try
        {
            command = parameters.at(0);
        }
        catch (out_of_range &e)
        {
            cerr << "Please enter a command.";
        }

        if (command.compare("addR") == 0) {
            // Add a rectangle to stored shapes.
            unsigned int x, y, h, w;
            // Note: Although it's normally bad practise to catch all exceptions under e, assignParameters and shape methods will only return exceptions we have created.
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), x, y, h, w);

                Rectangle* r = new Rectangle(x, y, h, w);
                shapes.push_back(r);
                cout << r;
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "addR <x:uint> <y:uint> <h:uint> <w:uint>";
            }
        }
        else if (command.compare("addS") == 0) {
            // Add a square to stored shapes.
            unsigned int x, y, e;
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), x, y, e);

                Square* s = new Square(x, y, e);
                shapes.push_back(s);
                cout << s;
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "addS <x:uint> <y:uint> <e:uint>";
            }
        }
        else if (command.compare("addC") == 0) {
            // Add a circle to stored shapes.
            unsigned int x, y, r;
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), x, y, r);

                Circle* c = new Circle(x, y, r);
                shapes.push_back(c);
                cout << c;
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "addC <x:uint> <y:uint> <r:uint>";
            }
        }
        else if (command.compare("scale") == 0) {
            // Scale the lengths of a stored shape by x y.
            unsigned int shapeNo;
            double scaleX, scaleY;
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), shapeNo, scaleX, scaleY);
                if (scaleX > 0 && scaleY > 0)
                {
                    Shape *s = findShape(shapes, shapeNo);

                    Movable *m = dynamic_cast<Movable*>(s);
                    m->scale(scaleX, scaleY);

                    cout << s;
                }
                else
                {
                    cerr << "Scaling factor must be greater than 0.";
                }
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "scale <shapeNo:uint> <scaleX:double> <scaleY:double>";
            }
        }
        else if (command.compare("move") == 0) {
            // Move a stored shape to x y.
            unsigned int shapeNo, x, y;
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), shapeNo, x, y);

                Shape *s = findShape(shapes, shapeNo);

                Movable *m = dynamic_cast<Movable*>(s);
                m->move(x, y);

                cout << s;
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "move <shapeNo:uint> <x:uint> <y:uint>";
            }
        }
        else if (command.compare("remove") == 0) {
            // Remove a shape from stored shapes.
            unsigned int shapeNo;
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), shapeNo);

                Shape *s = findShape(shapes, shapeNo);
                cout << s;
                delete s;
                shapes.erase(shapes.begin() + shapeNo - 1);
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "remove <shapeNo:uint>";
            }
        }
        else if (command.compare("show") == 0) {
            // Show a given stored shape.
            unsigned int shapeNo;
            try
            {
                assignParameters(parameters.begin() + 1, parameters.end(), shapeNo);

                Shape *s = findShape(shapes, shapeNo);
                cout << s;
            }
            catch (exception &e)
            {
                cout << e.what() << endl << "show <shapeNo:uint>";
            }
        }
        else if (command.compare("display") == 0) {
            // Show all stored shapes.
            for (Shape *shape : shapes)
            {
                cout << endl << shape;
            }
        }
        else if (!command.empty() && command.compare("exit") != 0) {
            cout << "No command named '" << command << "'.";
        }

        // Necessary postprocessing.
        parameters.clear();
        cout << endl << endl;
    }

    // Destroy any Shapes on the heap.
    // We could use smart pointers, which will free the memory itself, however we do it manually for demonstration purposes.
    for (Shape *s : shapes)
    {
        delete s;
    }
    // No need to clear vector as it'll only last 5 more lines anyway.

    cout << "Press any key to continue...";
    getchar();

    return 0;
}
