#include "Parser.h"

#include <string>
#include <sstream>
#include <cstring>
#include <exception>

#if defined(_MSC_VER)
void parseCommand(
        char const *cstr,
        std::vector<std::string> &parameters)
{
    // Source: From the Microsoft Documentation on strtok_s
    char *next_token = NULL;
    char *token = strtok_s(cstr, " ", &next_token);
    while (token)
    {
        parameters.push_back(std::string(token));
        token = strtok_s(NULL, " ", &next_token);
    }

    //delete cstr;
}
#elif defined(__STDC_LIB_EXT1__)
void parseCommand(
        char const *cstr,
        std::vector<std::string> &parameters)
{
    // Source: cppreference.com/w/c/string/byte/strtok
    size_t max = sizeof(cstr);
    char *next_token = NULL;
    char *token = strtok_s(cstr, &max, " ", &next_token);
    while (token)
    {
        parameters.push_back(std::string(token));
        token = strtok_s(NULL, &max, " ", &next_token);
    }
}
#endif

void parseCommand(
        std::string const &userCommand,
        std::vector<std::string> &parameters)
{
    // This function is used in main if _s C functions are not defined (e.g. when compiling with gcc or clang)
    std::string token;
    std::stringstream paramStream(userCommand);

    while (paramStream >> token)
    {
        parameters.push_back(token);
    }
}

unsigned int stou(std::string const &str, size_t *pos/*= nullptr*/, int base/*= 10*/)
{
    // https://stackoverflow.com/a/8715855
    unsigned long result = std::stoul(str, pos, base);
    if (result > std::numeric_limits<unsigned int>::max())
    {
        throw std::out_of_range("stou");
    }
    return result;
}

void assignParameters(
        std::vector<std::string>::iterator itParam,
        std::vector<std::string>::iterator end,
        unsigned int &param)
{
    if (itParam == end)
    {
        throw std::domain_error("Not enough arguments.");
    }

    try
    {
        param = stou(*itParam);
    }
    catch (std::invalid_argument &e)
    {
        std::stringstream what;
        what << "Bad argument: '" << *itParam << "'. Must be of type unsigned integer.";
        throw std::invalid_argument(what.str());
    }
    catch (std::out_of_range &e)
    {
        std::stringstream what;
        what << "Bad argument: '" << *itParam << "'. Value must be within "
             << std::numeric_limits<unsigned int>::min() << " and "
             << std::numeric_limits<unsigned int>::max() << '.';
        throw std::out_of_range(what.str());
    }
}

void assignParameters(
        std::vector<std::string>::iterator itParam,
        std::vector<std::string>::iterator end,
        double &param)
{
    if (itParam == end)
    {
        throw std::domain_error("Not enough arguments.");
    }

    try
    {
        param = stod(*itParam);
    }
    catch (std::invalid_argument &e)
    {
        std::stringstream what;
        what << "Bad argument: '" << *itParam << "'. Must be of type double.";
        throw std::invalid_argument(what.str());
    }
    catch (std::out_of_range &e)
    {
        std::stringstream what;
        what << "Bad argument: '" << *itParam << "'. Value must be within "
             << std::numeric_limits<double>::min() << " and "
             << std::numeric_limits<double>::max() << '.';
        throw std::out_of_range(what.str());
    }
}

/*
    Implemented in Parser.h, by order of template definitions:
    void assignParameters(
            std::vector<std::string>::iterator itParam,
            std::vector<std::string>::iterator end,
            T &firstParam,
            Args &... params);
*/
