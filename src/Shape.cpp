#include <vector>
#include <sstream>
#include <iomanip>
#include <limits>

#include "Shape.h"

Shape::Shape(unsigned int leftTopX, unsigned int leftTopY, bool const isCircular/*= false*/)
{
    this->isCircular = isCircular;

    this->leftTop = Point(leftTopX, leftTopY);
    this->rightBottom = Point(0, 0);

    // As points is a linked list, it is constant time to insert elements so technically alternating assignments is unnecessary.
    // However, I think this layout makes it most obvious what order the points are meant to be in and why.
    if (isCircular)
    {
        points = {&this->leftTop, &this->rightBottom};
    }
    else
    {
        this->rightTop = Point(0, 0);
        this->leftBottom = Point(0, 0);
        points = {&this->leftTop, &this->rightTop, &this->rightBottom, &this->leftBottom};
    }
}

std::string Shape::toString() const
{
    std::stringstream out;
    out << std::fixed; // Use fixed point notation for floating-point types.

    // Call hook for subtype specific information.
    out << this->typeString();

    out << "Points[";
    for (Point *p : this->points)
    {
        out << "(" << p->getX() << "," << p->getY() << ")";
    }
    out << "]" << std::endl;

    // We use setprecision to mimic brief output of 1dp.
    out << std::setprecision(1) << "Area=" << this->area << " ";
    out << std::setprecision(1) << "Perimeter=" << this->perimeter << std::endl;

    return out.str();
}

std::ostream &operator<<(std::ostream &os, Shape const *shape)
{
    os << shape->toString();
    return os;
}
