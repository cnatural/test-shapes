#include "Helpers.h"

#include <stdexcept>
#include <limits>

unsigned int addSafe(unsigned int lhs, unsigned int rhs)
{
    if (rhs > std::numeric_limits<unsigned int>::max() - lhs)
    {
        throw std::out_of_range("addSafe");
    }

    return lhs + rhs;
}

unsigned int dtouSafe(double d)
{
    if (d < std::numeric_limits<unsigned int>::min() || d > std::numeric_limits<unsigned int>::max())
    {
        throw std::out_of_range("dtouSafe");
    }

    return (unsigned int)d;
}
