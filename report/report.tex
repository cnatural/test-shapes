\documentclass[10.5pt, a4paper, usenames, dvipsnames]{article}
\usepackage[utf8]{inputenc}


\usepackage[a4paper, margin=20mm]{geometry}

\usepackage{titlesec}
\titleformat*{\section}{\large\bfseries}
\titlespacing{\section}{0pt}{6pt}{3pt}
\titleformat*{\subsection}{\normalsize\bfseries}
\titlespacing{\subsection}{0pt}{5pt}{2pt}
\titlespacing{\subsubsection}{0pt}{5pt}{2pt}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{CMP2801M Advanced Programming Item 1}
\rhead{Program Report}
\lfoot{Matthew Murr (17664330)}
\cfoot{}
\rfoot{\thepage}

\title{\vspace{-12mm}CMP2801M Advanced Programming Assessment 1}
\author{MUR17664330 Matthew Murr}
\date{}

\begin{document}

\maketitle
\thispagestyle{fancy}

\section{Application Design}

The program stores representations of shapes, according to commands from the
user. The commands include: addR, addS and addC, which add a rectangle, square
or circle to the stored shapes respectively; move, which sets the position of
the topLeft-most point of a shape; scale, which multiplies the x and y axis
lengths of a shape; remove, which erases a shape from storage; show, which
shows a specific shape; display, which shows all shapes; exit, which terminates
the program.

\subsection{Program Design}

\subsubsection{Changes from the Brief}
All integers used in the co-ordinate system are unsigned. This contains the
entire used range of an int without increasing memory requirements, guarantees
type safety for the co-ordinate system ensuring further runtime checks are
unnecessary or minimal, and gives a higher upper-bound of
usable co-ordinate space. Floats are replaced with doubles, as this is C++'s
default floating-point type and guarantees at least as much accuracy.

\subsubsection{Point}
\noindent
\begin{tabular}{|p{0.4\linewidth}|p{0.1\linewidth}|p{0.5\linewidth}|}
    \hline
    Declaration & Access & Purpose\\
    \hline
    unsigned int x, y & private & Co-ordinate values.\\
    \hline
    Point(unsigned int x, unsigned int y) & public &
    Initialises x and y.\\
    \hline
    unsigned int getX() const & protected & Getter for x.\\
    \hline
    unsigned int getY() const & public & Getter for y.\\
    \hline
    void move(unsigned int x, unsigned int y) & public & Mutator for
    x and y. Implemented as a single function to reduce overhead.\\
    \hline
\end{tabular}

\subsubsection{Shape}
\noindent
\begin{tabular}{|p{0.4\linewidth}|p{0.1\linewidth}|p{0.5\linewidth}|}
    \hline
    Point leftTop, rightTop, rightBottom, leftBottom & protected & 4 possible
    pairs of co-ordinates that represent the location of the shape.\\
    \hline
    list$<$Point*$>$ points & protected & Pointers to above Points.\\
    \hline
    bool isCircular & protected & \\
    \hline
    double area, perimeter & protected & \\
    \hline
    virtual string typeString() const = 0; & protected & Overridden by
    subclasses to give representation of subclass-specific data to toString.\\
    \hline
    Shape(unsigned int leftTopX, unsigned int leftTopY,
    bool const isCircular =
    false) & public & Initialises leftTop and isCircular, followed by points
    list and other points according to isCircular (leftTop and bottomRight if
    true, else all). Then calls subclass implementation of calculate methods to
    give other points correct values.\\
    \hline
    virtual $\sim$Shape() \{ \} & public & Ensures pointers to type shape can
    be deleted.\\
    \hline
    virtual void calculatePoints() = 0; & public & Moves points to satisfy the
    shape and leftTop.\\
    \hline
    virtual void calculateArea() = 0; & public & Sets area.\\
    \hline
    virtual void calculatePerimeter() = 0; & public & Sets perimeter.\\
    \hline
    virtual string toString() const & public & Returns string representation of
    class.\\
    \hline
\end{tabular}

\subsubsection{Movable}
\noindent
\begin{tabular}{|p{0.4\linewidth}|p{0.1\linewidth}|p{0.5\linewidth}|}
    \hline
    Declaration & Access & Purpose\\
    \hline
    virtual void move(unsigned int newX, unsigned int
    newY) = 0; & public & Pure virtual function for moving topLeft Point to
    newX newY and updating all other points.\\
    \hline
    virtual void scale(double scaleX, double scaleY) = 0;
    & public & Pure virtual function for increasing an object's dimensions
    across X and Y.\\
    \hline
\end{tabular}

\subsubsection{Rectangle, Square and Circle}
Implement virtual functions in Movable and Shape, detailed above. Contain
error checking throughout to ensure overflows do not occur on construction or
operations that change the position and dimensions of the shape. The calculate*
functions in Shape are called on construction and scaling. calculatePoints is
also called within move for all of them. All contain the same implementation
for move, however it was recommended that Movable remain a pure abstract class.

\subsubsection{Other}
\noindent
\begin{tabular}{|p{0.51\linewidth}|p{0.51\linewidth}|}
\hline
    ostream \&operator$<<$(ostream \&os, Shape const *shape) & Overloads the
    $<<$ operator, implementing os $<<$ shape-$>$toString.\\
    \hline
    void parseCommand(char *cstr, vector$<$string$>$ \&parameters) & Tokenises
    cstr and loads results into parameters. Uses strtok\_s.\\
    \hline
    void parseCommand(string const \&userCommand, vector$<$string$>$
    \&parameters) &
    Tokenises userCommand and loads result int parameters. Uses stringstream.
    For systems with no safe implementation of strtok.\\
    \hline
    void assignParameters(vector$<$string$>$::iterator itParam,
    vector$<$string$>$::iterator end, unsigned int \&param) & Assigns the
    parameter at itParam to the integer variable referenced by param.\\
    \hline
    void assignParameters(vector$<$string$>$::iterator itParam,
    vector$<$string$>$::iterator end, double \&param) & Assigns the
    parameter at itParam to the double variable referenced by param.\\
    \hline
    template $<$typename T, typename... Args$>$
    void assignParameters(vector$<$string$>$::iterator itParam,
    vector$<$string$>$::iterator end, T \&firstParam, Args \&... params) &
    Assigns the parameter at itParam to firstParam, and recurses with params.
    Uses variadic templates. Throws invalid argument if not unsigned int or
    double, out\_of\_range if not within numeric limits of type, and
    domain\_error if not enough params are given.\\
    \hline
    unsigned int addSafe(unsigned int lhs, unsigned int rhs) & Checks for
    overflow before adding.\\
    \hline
    unsigned int dtouSafe(double d) & Checks bounds before casting.\\
    \hline
\end{tabular}
\section{Evaluation}

\subsection{Testing}
There is not enough space to present comprehensive tests for each class and
method of the program. Here we show only blackbox tests through the console
interface.\\

\noindent
\begin{tabular}{|p{0.4\linewidth}|p{0.22\linewidth}|p{0.05\linewidth}|p{0.3\linewidth}|}
    \hline
    Test & Expected & Pass & Notes\\
    \hline
    addR/S/C Valid Value & Repr with correct values. & YES & \\
    \hline
    addR/S/C leftTop Beyond Max (addS 5000000000000 1 3) & Bad argument, too
    high. & YES & \\
    \hline
    addR/S/C leftTop Below Min (addC 7 -6 5) & Bad argument, too low. & YES & \\
    \hline
    addR/S/C Length Crosses Max Bound (addS 8 3 4294967290) & Cannot create
    shape. & YES & \\
    \hline
    move/scale Valid shapeNo (move 1 4 6) & Repr with correct values. & YES & \\
    \hline
    move/scale Invalid shapeNo (scale 4 2 2) & No shape at index. & YES & \\
    \hline
    move/scale Negative shapeNo (move -4 2 2) & Bad argument, too low. & YES & \\
    \hline
    move To Position Where Points Cross Max Bound (move 1 4294967295 0) &
    Cannot move. & YES & \\
    \hline
    scale Negative Amount (scale 1 4 -3) & Scaling factor must be a positive value. & YES &\\
    \hline
    scale Square By Differing X/Y Values (scale 1 4 5) & Scaling a square must be isotropic.
    & YES & \\
    \hline
    No Command Given () & Please enter a command. & YES & \\
    \hline
    No Arguments Given (addR) & Not enough arguments. & YES & \\
    \hline
    Insufficient Arguments Given (addR 1, addR 1 1) & Not enough arguments. & YES
    & \\
    \hline
    Too Many Arguments Given (addR 1 1 1 1 1) & Too many arguments. & NO &
    Extra arguments are not
    regarded.\\
    \hline
    No Leaked Memory After Allocation (exit) & No memory leaks. & YES &
    Checked with Valgrind\\
    \hline
\end{tabular}

\subsection{Time Complexity}
n is assumed to be the
number of shapes that the user creates, and all functions that do not interact
with the shapes are known to be constant time. We assume that the user
will call an add* command at least once -- construction of any shape is O(1),
and appending it to the vector of shapes is O(1), unless the new size of the
vector is larger than the memory allocated. Reallocation of the vector is O(n).
The remove, show, scale and move commands are all O(1), as no search occurs
along the vector
of shapes and access complexity is constant. The display command is O(n), as it
prints all existing shapes. Hence, for the total program, time complexity is
O(n) linear time regardless of the commands that the user executes.

\end{document}
