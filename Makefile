CC := g++
SRCDIR := src
BUILDDIR := build
TESTDIR := test
TARGET := bin/shapes

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name "*.$(SRCEXT)")
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -g -Wall
INC := -I include

$(TARGET): $(OBJECTS)
	@echo " $(CC) $^ -o $(TARGET)"; $(CC) $^ -o $(TARGET)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(dir $@)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

.PHONY: clean tests run

run:
	$(TARGET)

clean:
	@echo " $(RM) $(OBJECTS) $(TARGET)"; $(RM) $(OBJECTS) $(TARGET)

# Tests
tests:
	$(CC) $(CFLAGS) $(TESTDIR)/shapetests.cpp $(INC) -o bin/shapetests
